﻿B4J=true
Group=Default Group
ModulesStructureVersion=1
Type=Class
Version=4.2
@EndOfDesignText@
Sub Class_Globals
  Dim App As AWTRIX
  Dim weekday As Int
  Dim ypos As Int

  Dim line As String
  ' used to select datetime format text when multiple lines are provided
  Dim LoopIndex As Int = 0
  
  Dim TickCount As Int
  Dim Lines() As String
  
  Dim LINES_DEFAULT As String = "%2a %d.%m.;%T;KW %V.%Y"
End Sub

'Initializes the object. You can NOT add parameters to this method!
Public Sub Initialize() As String
  
  App.Initialize(Me,"App")
  
  'change plugin name (must be unique, avoid spaces)
  App.Name = "DateTime1o"
  
  'Version of the App
  App.Version = "21.51"
  
  'Description of the App. You can use HTML to format it
  App.Description = "Shows date, time depending on given format text."
  App.Author = "o1o99@o1o99.de"
  App.CoverIcon = 13
    
  'SetupInstructions. You can use HTML to format it
  'Supports mulipte lines for date time long.
  'e.g. <dayname>, <day>.<month>:<hour>:<minute>:
  App.setupDescription= $"
  <b>ShowDayBar[true/false]:</b>  Whether a weekdaybar should be displayed or not.<br />
  <b>Datetimeformat:</b> Controls which data of time and date are shown. Separate multiple lines with ';' which are shown one after the other. Check out C strftime function for format details.<br>
  <b>StartsSunday[true/false]:</b> Week begins on sunday.<br />
  "$
  
  'Tickinterval in ms (should be 65 by default)
  App.Tick = 65
  
  'needed Settings for this App (Wich can be configurate from user via webinterface)
  App.Settings = CreateMap("ShowDayBar":True, "StartsSunday":False, "Datetimeformat":LINES_DEFAULT)
  
  App.MakeSettings
  Return "AWTRIX20"
End Sub


' must be available
public Sub GetNiceName() As String
  Return App.Name
End Sub


' ignore
public Sub Run(Tag As String, Params As Map) As Object
  Return App.interface(Tag,Params)
End Sub

Sub App_Started
  TickCount = 0
  ypos = getNextYPos(-1)
  
  weekday=GetWeekNumber(DateTime.Now)
  If App.get("StartsSunday") Then
    weekday=weekday+1
  End If

  ' get datetime format text and split
  line = App.get("Datetimeformat")
  If line.Length > 0 Then
    Lines = Regex.Split(";", line)
  Else
    Lines = Regex.Split(";", LINES_DEFAULT)
  End If
      
End Sub

Sub App_Exited
  LoopIndex = LoopIndex + 1
End Sub

Sub App_settingsChanged
  TickCount = 0
  ypos = getNextYPos(-1)
  
  ' get datetime format text and split
  line = App.get("Datetimeformat")
  If line.Length > 0 Then
    Lines = Regex.Split(";", line)
  Else
    Lines = Regex.Split(";", LINES_DEFAULT)
  End If
  
End Sub

Sub App_genFrame
  TickCount = TickCount + App.Tick
    
  Dim now As Long = DateTime.Now
  Dim outline As String = ""
  
  If LoopIndex >= Lines.Length Then
    LoopIndex = 0
  End If
  line = Lines(LoopIndex)
  

  ' http://www.cplusplus.com/reference/ctime/strftime/
  If line.Contains("%") Then
    ' %a  Abbreviated weekday name *  Thu
    line = replaceAbbrWeekdayname(line, now)
    ' %A  Full weekday name * Thursday
    line = replaceFullWeekdayname(line, now)
    ' %b  Abbreviated month name *  Aug
    line = replaceAbbrMonthname(line, now)
    ' %B  Full month name * August
    line = replaceFullMonthname(line, now)
    ' %c  Date And time representation *  Thu Aug 23 14:55:02 2001
    line = replaceDateAndTime(line, now)
    ' %C  Year divided by 100 And truncated To integer (00-99)  20
    line = replaceYear100(line, now)
    ' %d  Day of the month, zero-padded (01-31) 23
    line = replaceDayOfMonthZero(line, now)
    ' %D  Short MM/DD/YY date, equivalent To %m/%d/%y 08/23/01
    line = replaceShortMMDDYY(line, now)
    ' %e  Day of the month, space-padded ( 1-31)  23
    line = replaceDayOfMonthSpace(line, now)
    ' %F  Short YYYY-MM-DD date, equivalent To %Y-%m-%d 2001-08-23
    line = replaceShortYYYYMMDD(line, now)
    ' %g  Week-based year, last two digits (00-99)  01
    line = replaceShortWeekYear(line, now)
    ' %G  Week-based year 2001
    line = replaceWeekYear(line, now)
    ' %h  Abbreviated month name * (same As %b) Aug
    line = replaceAbbrMonthName_h(line, now)
    ' %H  Hour in 24h format (00-23)  14
    line = replaceHour24H(line, now)
    ' %I  Hour in 12h format (01-12)  02
    line = replaceHour12H(line, now)
    ' %j  Day of the year (001-366) 235
    line = replaceDayOfYear(line, now)
    ' %m  Month As a decimal number (01-12) 08
    line = replaceMonth(line, now)
    ' %M  Minute (00-59)  55
    line = replaceMinute(line, now)
    ' %n  New-Line character ('\n')
    ' %p  AM Or PM designation  PM
    line = replaceAMPM(line, now)
    ' %r  12-hour clock time *  02:55:02 pm
    line = replaceHour24HClocktime(line, now)
    ' %R  24-hour HH:MM time, equivalent To %H:%M 14:55
    line = replaceHour12HClockTime(line, now)
    ' %S  Second (00-61)  02
    line = replaceSecond(line, now)
    ' %t  Horizontal-TAB character ('\t')
    ' %T  ISO 8601 time format (HH:MM:SS), equivalent To %H:%M:%S 14:55:02
    line = replaceISO8601(line, now)
    ' %u  ISO 8601 weekday As number with Monday As 1 (1-7) 4
    line = replaceISO8601WeekdayMonday(line, now)
    ' %U  Week number with the first Sunday As the first day of week one (00-53)  33
    ' %V  ISO 8601 week number (01-53)  34
    line = replaceISO8601Week(line, now)
    ' %w  weekday As a decimal number with Sunday As 0 (0-6)  4
    line = replaceWeekdaySunday(line, now)
    ' %W  Week number with the first Monday As the first day of week one (00-53)  34
    ' %x  Date representation * 08/23/01
    line = replaceDateMM_DD_YY(line, now)
    ' %X  Time representation * 14:55:02
    line = replaceTimeHHMMSS(line, now)
    ' %y  Year, last two digits (00-99) 01
    line = replaceShortYear(line, now)
    ' %Y  Year  2001
    line = replaceYear(line, now)

    outline = line
    'Log("Result[" & SceneIndex & "]: " & outline)
  End If
  
  Dim matrixlength As Int = getTextLengthAtMatrix(outline)
  If matrixlength < 33 Then
    App.genText(outline, False, ypos, Null, False)
    ypos = getNextYPos(ypos)
  Else
    App.genText(outline, False, 1, Null, False)
  End If
    
  If App.get("ShowDayBar") Then
    App.drawLine(0,7,31,7,Array As Int(0,0,0))
    For i=0 To 6
      If i=weekday-1  Then
        App.drawLine(3+i*4, 7, i*4+5, 7, Null)
      Else
        App.drawLine(3+i*4,7,i*4+5,7,Array As Int(80,80,80))
      End If
    Next
  End If
End Sub

Private Sub getNextYPos(current As Int) As Int
  ' 0..8
  Dim result As Int = current
  If current = -1 Then
    result = 9
  Else If current > 0 Then
    result = current - 1
  End If 
  Return result
End Sub

Private Sub getTextLengthAtMatrix(text As String) As Int
  Dim idx As Int
  Dim result As Int = 0
  
  For idx = 0 To text.Length - 1
    Select Asc(text.CharAt(idx))
      ' space=32; etc. .:;
      Case 32
        result = result + 2
      Case 33,43,44,45,46,58,59
        result = result + 3
        'M,Q,W...
      Case 77,81,87
        result = result + 6
      Case Else
        result = result + 4
    End Select
  Next
  
  Return result
End Sub

Private Sub GetWeekNumber(DateTicks As Long) As Int
  Dim psCurrFmt As String = DateTime.DateFormat
  DateTime.DateFormat = "u"
  Dim piCurrentWeek As Int = DateTime.Date(DateTicks)
  DateTime.DateFormat = psCurrFmt
  Return piCurrentWeek
End Sub

' %a  Abbreviated weekday name *  Thu
Private Sub replaceAbbrWeekdayname(in As String, now As Long) As String
  Dim name As String = DateUtils.GetDayOfWeekName(now)
  Dim result As String
  
  If in.Contains("%2a") Then
    result = in.Replace("%2a", name.SubString2(0,2))
  Else
    result = in.Replace("%a", name.SubString2(0,3))
  End If
  
  Return result
End Sub

' %A  Full weekday name * Thursday
Private Sub replaceFullWeekdayname(in As String, now As Long) As String
  Dim name As String = DateUtils.GetDayOfWeekName(now)
  Dim result As String = in.Replace("%A", name)
  Return result
End Sub

' %b  Abbreviated month name *  Aug
Private Sub replaceAbbrMonthname(in As String, now As Long) As String
  Dim name As String = DateUtils.GetMonthName(now)
  Dim result As String
  
  If in.Contains("%2b") Then
    result = in.Replace("%2b", name.SubString2(0,2))
  Else
    result = in.Replace("%b", name.SubString2(0,3))
  End If
  
  Return result
End Sub

' %B  Full month name * August
Private Sub replaceFullMonthname(in As String, now As Long) As String
  Dim name As String = DateUtils.GetMonthName(now)
  Dim result As String = in.Replace("%B", name)
  Return result
End Sub

' %c  Date And time representation *  Thu Aug 23 14:55:02 2001
Private Sub replaceDateAndTime(in As String, now As Long) As String
  Dim day As String = DateUtils.GetDayOfWeekName(now)
  Dim month As String = DateUtils.GetMonthName(now)
  
  DateTime.TimeFormat = "HH:mm:ss yyyy"
  Dim time As String = DateTime.Time(now)
  
  Dim result As String = in.Replace("%c", day.SubString2(0,3) & " " & month.SubString2(0,3) & " " & time)
  Return result
End Sub

' %C  Year divided by 100 And truncated To integer (00-99)  20
Private Sub replaceYear100(in As String, now As Long) As String
  DateTime.TimeFormat = "YY"
  Dim result As String = in.Replace("%C", DateTime.Time(now))
  Return result
End Sub

' %d  Day of the month, zero-padded (01-31) 23
Private Sub replaceDayOfMonthZero(in As String, now As Long) As String
  Dim month As String=NumberFormat(DateTime.GetDayOfMonth(now), 2, 0)
  Dim result As String = in.Replace("%d", month)
  Return result
End Sub

' %D  Short MM/DD/YY date, equivalent To %m/%d/%y 08/23/01
Private Sub replaceShortMMDDYY(in As String, now As Long) As String
  Dim day As String = NumberFormat(DateTime.GetDayOfMonth(now), 2, 0)
  Dim month As String = NumberFormat(DateTime.GetMonth(now), 2, 0)
  
  DateTime.TimeFormat = "yy"
  Dim yy As String = DateTime.Time(now)
  
  Dim result As String = in.Replace("%D", month & "/" & day & "/" & yy)
  Return result
End Sub

' %e  Day of the month, space-padded ( 1-31)  23
Private Sub replaceDayOfMonthSpace(in As String, now As Long) As String
  Dim day As String = NumberFormat(DateTime.GetDayOfMonth(now), 0, 0)
  If day.Length = 1 Then
    day = " " & day
  End If
  Dim result As String = in.Replace("%e", day)
  Return result
End Sub

' %F  Short YYYY-MM-DD date, equivalent To %Y-%m-%d 2001-08-23
Private Sub replaceShortYYYYMMDD(in As String, now As Long) As String
  Dim day As String = NumberFormat(DateTime.GetDayOfMonth(now), 2, 0)
  Dim month As String = NumberFormat(DateTime.GetMonth(now), 2, 0)
  Dim year As String = DateTime.GetYear(now)

  Dim result As String = in.Replace("%F", year & "-" & month & "-" & day)
  Return result
End Sub

' %g  Week-based year, last two digits (00-99)  01
Private Sub replaceShortWeekYear(in As String, now As Long) As String
  DateTime.TimeFormat = "YY"
  Dim result As String = in.Replace("%g", DateTime.Time(now))
  Return result
End Sub

' %G  Week-based year 2001
Private Sub replaceWeekYear(in As String, now As Long) As String
  DateTime.TimeFormat = "YYYY"
  Dim result As String = in.Replace("%G", DateTime.Time(now))
  Return result
End Sub

' %h  Abbreviated month name * (same As %b) Aug
Private Sub replaceAbbrMonthName_h(in As String, now As Long) As String
  Dim name As String = DateUtils.GetDayOfWeekName(now)
  Dim result As String = in.Replace("%h", name.SubString2(0,3))
  Return result
End Sub

' %H  Hour in 24h format (00-23)  14
Private Sub replaceHour24H(in As String, now As Long) As String
  DateTime.TimeFormat = "HH"
  Dim data As String = DateTime.Time(now)
  Dim result As String = in.Replace("%H", data)
  Return result
End Sub

' %I  Hour in 12h format (01-12)  02
Private Sub replaceHour12H(in As String, now As Long) As String
  DateTime.TimeFormat = "KK"
  Dim result As String = in.Replace("%I", DateTime.Time(now))
  Return result
End Sub

' %j  Day of the year (001-366) 235
Private Sub replaceDayOfYear(in As String, now As Long) As String
  DateTime.TimeFormat = "D"
  Dim result As String = in.Replace("%j", DateTime.Time(now))
  Return result
End Sub

' %m  Month As a decimal number (01-12) 08
Private Sub replaceMonth(in As String, now As Long) As String
  Dim name As String = NumberFormat(DateTime.GetMonth(now), 2, 0)
  Dim result As String = in.Replace("%m", name)
  Return result
End Sub

' %M  Minute (00-59)  55
Private Sub replaceMinute(in As String, now As Long) As String
  DateTime.TimeFormat = "mm"
  Dim result As String = in.Replace("%M", DateTime.Time(now))
  Return result
End Sub

' %n  New-Line character ('\n')
' %p  AM Or PM designation  PM
Private Sub replaceAMPM(in As String, now As Long) As String
  DateTime.TimeFormat = "a"
  Dim result As String = in.Replace("%p", DateTime.Time(now))
  Return result
End Sub

' %r  12-hour clock time *  02:55:02 pm
Private Sub replaceHour12HClockTime(in As String, now As Long) As String
  DateTime.TimeFormat = "KK:mm:ss a"
  Dim result As String = in.Replace("%r", DateTime.Time(now))
  Return result
End Sub

' %R  24-hour HH:MM time, equivalent To %H:%M 14:55
Private Sub replaceHour24HClocktime(in As String, now As Long) As String
  DateTime.TimeFormat = "HH:mm"
  Dim result As String = in.Replace("%R", DateTime.Time(now))
  Return result
End Sub

' %S  Second (00-61)  02
Private Sub replaceSecond(in As String, now As Long) As String
  DateTime.TimeFormat = "ss"
  Dim result As String = in.Replace("%S", DateTime.Time(now))
  Return result
End Sub

' %t  Horizontal-TAB character ('\t')
' %T  ISO 8601 time format (HH:MM:SS), equivalent To %H:%M:%S 14:55:02
Private Sub replaceISO8601(in As String, now As Long) As String
  DateTime.TimeFormat = "HH:mm:ss"
  Dim result As String = in.Replace("%T", DateTime.Time(now))
  Return result
End Sub

' %u  ISO 8601 weekday As number with Monday As 1 (1-7) 4
Private Sub replaceISO8601WeekdayMonday(in As String, now As Long) As String
  DateTime.TimeFormat = "u"
  Dim result As String = in.Replace("%u", DateTime.Time(now))
  Return result
End Sub

' %U  Week number with the first Sunday As the first day of week one (00-53)  33
' %V  ISO 8601 week number (01-53)  34
Private Sub replaceISO8601Week(in As String, now As Long) As String
  DateTime.TimeFormat = "w"
  Dim result As String = in.Replace("%V", DateTime.Time(now))
  Return result
End Sub

' %w  weekday As a decimal number with Sunday As 0 (0-6)  4
Private Sub replaceWeekdaySunday(in As String, now As Long) As String
  Dim name As String = DateUtils.GetDayOfWeekName(now)
  Dim result As String = in.Replace("%w", name)
  Return result
End Sub

' %W  Week number with the first Monday As the first day of week one (00-53)  34
' %x  Date representation * 08/23/01
Private Sub replaceDateMM_DD_YY(in As String, now As Long) As String
  DateTime.DateFormat = "MM/dd/YY"
  Dim result As String = in.Replace("%x", DateTime.Date(now))
  Return result
End Sub

' %X  Time representation * 14:55:02
Private Sub replaceTimeHHMMSS(in As String, now As Long) As String
  DateTime.TimeFormat = "HH:mm:ss"
  Dim result As String = in.Replace("%X", DateTime.Time(now))
  Return result
End Sub

' %y  Year, last two digits (00-99) 01
Private Sub replaceShortYear(in As String, now As Long) As String
  DateTime.TimeFormat = "yy"
  Dim result As String = in.Replace("%y", DateTime.Time(now))
  Return result
End Sub

' %Y  Year  2001
Private Sub replaceYear(in As String, now As Long) As String
  DateTime.TimeFormat = "yyyy"
  Dim result As String = in.Replace("%Y", DateTime.Time(now))
  Return result
End Sub